-- This file needs to have same structure as nvconfig.lua
-- https://github.com/NvChad/ui/blob/v2.5/lua/nvconfig.lua

-- Source custom autocommands
require "autocmds"

---@type ChadrcConfig
local M = {}

M.base46 = {
  theme = "onedark",

  -- theme = "kanagawa",
  -- hl_override = {
  --     Visual = { bg = "lightbg" },
  -- }
}

return M
